package com.prueba.mx.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name="bitacora")
@AllArgsConstructor
@NoArgsConstructor
public class Bitacora {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name="peticionesConsultadas")
    private Integer peticionesConsultadas;

    @Column(name="insercciones")
    private Integer insercciones;

}
