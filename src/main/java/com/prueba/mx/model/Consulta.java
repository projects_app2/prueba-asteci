package com.prueba.mx.model;

import lombok.Data;

@Data
public class Consulta {
    private Integer consultado;
    private Integer insertados;
    private Integer resultos;
    private Integer abiertos;
}
