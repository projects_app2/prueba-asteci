package com.prueba.mx.model;

import lombok.Data;

@Data
public class ResponseApi {
    private String message;
    private Object data;
}
