package com.prueba.mx.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="incendencia")
public class Incidencia {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name="nombreEquipo")
    private String nombreEquipo;

    @Column(name="marca")
    private String marca;

    @Column(name="fecha")
    private Date fecha;

    @Column(name="estatus")
    private Integer estatus;

    @Column(name="operador")
    private String operador;

}
