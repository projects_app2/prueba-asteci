package com.prueba.mx.controller;


import com.prueba.mx.model.Consulta;
import com.prueba.mx.model.Incidencia;
import com.prueba.mx.model.ResponseApi;
import com.prueba.mx.service.IncidenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/api")
public class IncidenciasController {

    @Autowired
    IncidenciaService incidenciaService;

    @GetMapping("/consultas")
    public ResponseEntity<?> getConsultas( ) {

        Consulta consulta = incidenciaService.getConsultasBitacora();
        ResponseApi response = new ResponseApi();
        response.setMessage("OK");
        response.setData(consulta);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/incidentes")
    public ResponseEntity<?> postIncidentes(@RequestParam(required = false,name="dia") String fecha , @RequestParam(required = false,name="operador") String operador) throws ParseException {
        ResponseApi response = new ResponseApi();

        List<Incidencia> incidencias = incidenciaService.getIncidencias(fecha,operador);

        response.setMessage("OK");
        response.setData(incidencias);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }



    @PostMapping("/incidentes")
    public ResponseEntity<?> postIncidentes(@RequestBody Incidencia request) {
        ResponseApi response = new ResponseApi();
        Incidencia incidencia = incidenciaService.saveIncidencia(request);
        response.setMessage("OK");
        response.setData(incidencia);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PutMapping("/incidentes")
    public ResponseEntity<?> pustIncidentes(@RequestBody Incidencia request) {
        ResponseApi response = new ResponseApi();
        Incidencia incidencia = incidenciaService.updateIncidencia(request);
        response.setMessage("OK");
        response.setData(incidencia);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}