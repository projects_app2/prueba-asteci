package com.prueba.mx.service;

import com.prueba.mx.model.Consulta;
import com.prueba.mx.model.Incidencia;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface IncidenciaService {

    Consulta getConsultasBitacora();
    List<Incidencia> getIncidencias(String fecha, String operador) throws ParseException;

    Incidencia saveIncidencia(Incidencia incidencia) ;

    Incidencia updateIncidencia(Incidencia incidencia) ;

}
