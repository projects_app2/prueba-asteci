package com.prueba.mx.service.impl;

import com.prueba.mx.model.Bitacora;
import com.prueba.mx.repository.BitacoraRepository;
import com.prueba.mx.service.BitacoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BitacoraServiceImpl implements BitacoraService {

    @Autowired
    BitacoraRepository bitacoraRepository;

    @Override
    public Bitacora saveBitacora(Bitacora bitacora) {
        return bitacoraRepository.save(bitacora);
    }

    @Override
    public List<Bitacora> getBitacora() {
        return bitacoraRepository.findAll();
    }
}
