package com.prueba.mx.service.impl;

import com.prueba.mx.model.Bitacora;
import com.prueba.mx.model.Consulta;
import com.prueba.mx.model.Incidencia;
import com.prueba.mx.repository.IncidenciaRepository;
import com.prueba.mx.service.BitacoraService;
import com.prueba.mx.service.IncidenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("incidenciasService")
public class IncidenciaServiceImpl implements IncidenciaService {

    @Autowired
    IncidenciaRepository incidenciaRepository;

    @Autowired
    BitacoraService bitacoraService;

    @Override
    public Consulta getConsultasBitacora() {
        List<Bitacora> bitacoras = bitacoraService.getBitacora();
        Consulta consulta = new Consulta();
        if(bitacoras.isEmpty()){
            consulta.setInsertados(0);
            consulta.setConsultado(0);
        }else{
            consulta.setInsertados(bitacoras.get(0).getInsercciones());
            consulta.setConsultado(bitacoras.get(0).getPeticionesConsultadas());
        }

        //Estatus 0 abiertas
        consulta.setAbiertos(incidenciaRepository.countAllByEstatus(0));

        //Estatus 1 cerrados
        consulta.setResultos(incidenciaRepository.countAllByEstatus(1));

        return consulta;
    }

    @Override
    public List<Incidencia> getIncidencias(String fecha, String operador) throws ParseException {

        aumentarConsultaBitacora();
        List<Incidencia> listaIncidencias = new ArrayList<>();
        if(fecha != null && operador != null){
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);

            return incidenciaRepository.findAllByFechaAndOperador(date1,operador);
        }else if(fecha != null && operador == null){
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
            return incidenciaRepository.findAllByFecha(date1);
        }else if(fecha == null && operador != null){
           return  incidenciaRepository.findAllByOperador(operador);
        }



        return incidenciaRepository.findAll();

    }

    @Override
    public Incidencia saveIncidencia(Incidencia incidencia) {
        aumentarInsercciones();
        return incidenciaRepository.save(incidencia);
    }

    @Override
    public Incidencia updateIncidencia(Incidencia incidencia) {
        return incidenciaRepository.save(incidencia);
    }

    private void aumentarConsultaBitacora(){
        List<Bitacora> bitacoras = bitacoraService.getBitacora();

        if(bitacoras.isEmpty()){
            initBitacora(bitacoras,1);
            return;
        }

        Bitacora bitacora = bitacoras.get(0);
        Integer peticiones = bitacora.getPeticionesConsultadas();
        bitacora.setPeticionesConsultadas(peticiones + 1);
        bitacoraService.saveBitacora(bitacora);
    }

    private void aumentarInsercciones(){
        List<Bitacora> bitacoras = bitacoraService.getBitacora();

        if(bitacoras.isEmpty()){
            initBitacora(bitacoras,0);
            return;
        }

        Bitacora bitacora = bitacoras.get(0);
        Integer insercciones = bitacora.getInsercciones();
        bitacora.setInsercciones(insercciones + 1);
        bitacoraService.saveBitacora(bitacora);
    }

    private void initBitacora(List<Bitacora> bitacoras, Integer aumentoCaso){
        Bitacora bitacoraSave = new Bitacora();
        bitacoraSave.setPeticionesConsultadas(aumentoCaso == 1 ? 1 : 0);
        bitacoraSave.setInsercciones(aumentoCaso == 0 ? 1 : 0);
        bitacoraService.saveBitacora(bitacoraSave);
    }

}
