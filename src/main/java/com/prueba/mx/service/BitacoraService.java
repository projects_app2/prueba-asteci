package com.prueba.mx.service;

import com.prueba.mx.model.Bitacora;

import java.util.List;

public interface BitacoraService {

    Bitacora saveBitacora(Bitacora bitacora);
    List<Bitacora> getBitacora();

}
