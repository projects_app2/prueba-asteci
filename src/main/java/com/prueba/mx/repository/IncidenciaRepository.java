package com.prueba.mx.repository;

import com.prueba.mx.model.Incidencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IncidenciaRepository extends JpaRepository<Incidencia, Long> {

    List<Incidencia> findAllByFechaAndOperador(Date fecha, String operador);
    List<Incidencia> findAllByFecha(Date fecha);
    List<Incidencia> findAllByOperador(String operador);

    Integer countAllByEstatus(Integer estatus);

}
